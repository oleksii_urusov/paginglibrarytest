package com.example.oleksiiurusov.paginglibrarytest.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.oleksiiurusov.paginglibrarytest.R

private const val VIEW_TYPE_REGULAR = 0
private const val VIEW_TYPE_LOADING = 1
private const val VIEW_TYPE_RETRY = 2

abstract class PagingListAdapter<TYPE>(
    diffCallback: DiffUtil.ItemCallback<TYPE>,
    private val retryClickListener: () -> Unit,
    private val showLoadingWhenEmpty: Boolean = false,
    private val showRetryWhenEmpty: Boolean = false
) : PagedListAdapter<TYPE, RecyclerView.ViewHolder>(diffCallback) {

    private var state: State = State.Idle

    private val needExtraRow
        get() = state is State.Retry || state is State.Loading

    private val isStateNotIdleWhileListEmpty
        get() = itemCount == 1 && state !is State.Idle

    protected abstract fun onCreateItemViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    protected open fun onCreateRetryViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_default_retry, parent, false)

        return RetryViewHolder(
            view,
            retryClickListener
        )
    }

    protected open fun onCreateLoadingViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_default_loading, parent, false)
        return LoadingViewHolder(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        VIEW_TYPE_LOADING -> onCreateLoadingViewHolder(parent)
        VIEW_TYPE_RETRY -> onCreateRetryViewHolder(parent)
        else -> onCreateItemViewHolder(parent)
    }

    override fun onCurrentListChanged(previousList: PagedList<TYPE>?, currentList: PagedList<TYPE>?) {
        super.onCurrentListChanged(previousList, currentList)
        if (currentList?.size == 0 && state is State.Retry && !showRetryWhenEmpty) {
            setIdleState()
        }
    }

    fun setRetryState() {
        if (itemCount == 0 && !showRetryWhenEmpty) {
            return
        }

        if (isStateNotIdleWhileListEmpty && !showRetryWhenEmpty) {
            setIdleState()
            return
        }

        state = State.Retry
        notifyItemChanged(itemCount - 1)
    }

    fun setLoadingState() {
        if (itemCount == 0 && !showLoadingWhenEmpty) {
            return
        }

        if (isStateNotIdleWhileListEmpty && !showLoadingWhenEmpty) {
            setIdleState()
            return
        }

        state = State.Loading
        notifyItemChanged(itemCount - 1)
    }

    fun setIdleState() {
        if (state !is State.Idle) {
            state = State.Idle
            notifyItemRemoved(itemCount)
        }
    }

    override fun getItemViewType(position: Int) = when {
        position == itemCount - 1 && state is State.Loading -> VIEW_TYPE_LOADING
        position == itemCount - 1 && state is State.Retry -> VIEW_TYPE_RETRY
        else -> VIEW_TYPE_REGULAR
    }

    override fun getItemCount() = super.getItemCount() + if (needExtraRow) 1 else 0
}

private sealed class State {
    object Idle : State()
    object Loading : State()
    object Retry : State()
}

class LoadingViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView)

class RetryViewHolder(
    view: View,
    onRetryClickListener: () -> Unit
) : RecyclerView.ViewHolder(view) {
    init {
        view.findViewById<View>(R.id.list_is_empty_retry).setOnClickListener { onRetryClickListener() }
    }
}