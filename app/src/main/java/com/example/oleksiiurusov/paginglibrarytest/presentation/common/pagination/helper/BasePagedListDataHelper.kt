package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.oleksiiurusov.paginglibrarytest.common.CustomTransformations
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.DataFetchDelegate
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedData
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedDataBuilder

abstract class BasePagedListDataHelper<MODEL, RESPONSE_MODEL>(
    private val pagedDataBuilder: PagedDataBuilder
) : DataFetchDelegate<MODEL> {

    private val pagedData = MutableLiveData<PagedData<MODEL>>()
    val listState: LiveData<ListStateUI> =
        CustomTransformations.nonNullSwitchMap(
            pagedData,
            ListStateUI.Idle(true)
        ) {
            it.loadingManager.loadingState
        }

    protected abstract fun fetch(
        startPosition: Int,
        loadSize: Int,
        isInitialLoad: Boolean,
        onSuccess: (RESPONSE_MODEL) -> Unit,
        onError: () -> Unit
    )

    protected abstract fun RESPONSE_MODEL.mapToModelList(): List<MODEL>

    protected fun getUpdatedPagedData(): PagedData<MODEL> =
        pagedDataBuilder.build(this)
            .also {
                pagedData.value = it
            }

    override fun requestPageData(
        startPosition: Int,
        loadSize: Int,
        isInitialLoad: Boolean,
        onSuccess: (List<MODEL>) -> Unit,
        onError: () -> Unit
    ) {
        fetch(
            startPosition,
            loadSize,
            isInitialLoad,
            { onSuccess(it.mapToModelList()) },
            onError
        )
    }

    fun retry() {
        pagedData.value?.loadingManager?.retryFailedRequest()
    }

    fun reload() {
        pagedData.value?.loadingManager?.reload()
    }
}