package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedDataBuilder

class SimplePagedListDataHelper<MODEL>
constructor(
    pagedDataBuilder: PagedDataBuilder
) : PagedListDataHelper<MODEL, List<MODEL>>(pagedDataBuilder) {

    fun createPagedLiveData(
        getData: (
            pageParams: PageParameters,
            callback: (ResultObject<List<MODEL>>) -> Unit
        ) -> Unit
    ): LiveData<PagedList<MODEL>> {
        this.dataFetchFunction = getData
        return getUpdatedPagedData().data
    }

    override fun List<MODEL>.mapToModelList(): List<MODEL> = this
}