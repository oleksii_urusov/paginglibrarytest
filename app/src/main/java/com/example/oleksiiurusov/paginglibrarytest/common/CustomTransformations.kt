package com.example.oleksiiurusov.paginglibrarytest.common

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer

object CustomTransformations {
    @MainThread
    fun <X, Y> nonNullSwitchMap(
        source: LiveData<X>,
        initialValue: Y,
        switchMapFunction: (X) -> LiveData<Y>
    ): LiveData<Y> {
        val result = MediatorLiveData<Y>().apply {
            value = initialValue
        }
        result.addSource(source, object : Observer<X> {
            var mSource: LiveData<Y>? = null

            override fun onChanged(x: X?) {
                if (x == null) {
                    return
                }

                val newLiveData = switchMapFunction(x)
                if (mSource === newLiveData) {
                    return
                }

                mSource?.let {
                    result.removeSource(it)
                }
                mSource = newLiveData
                mSource?.let {
                    result.addSource(it) { y -> result.value = y }
                }
            }
        })
        return result
    }
}
