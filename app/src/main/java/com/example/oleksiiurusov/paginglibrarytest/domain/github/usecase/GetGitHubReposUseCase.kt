package com.example.oleksiiurusov.paginglibrarytest.domain.github.usecase

import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.domain.common.usecase.PaginatedUseCase
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters
import com.example.oleksiiurusov.paginglibrarytest.domain.github.repository.GitHubRepository
import kotlinx.coroutines.CoroutineScope

class GetGitHubReposUseCase(private val gitHubRepository: GitHubRepository) :
    PaginatedUseCase<ResultObject<List<GitHubRepo>>>() {

    override suspend fun executeRequest(
        request: PageParameters,
        coroutineScope: CoroutineScope
    ): ResultObject<List<GitHubRepo>> = gitHubRepository.getRepositories(request)
}