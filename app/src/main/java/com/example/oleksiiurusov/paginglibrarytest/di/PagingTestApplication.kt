package com.example.oleksiiurusov.paginglibrarytest.di

import android.app.Application

class PagingTestApplication : Application() {

    lateinit var injector: Injector
        private set

    override fun onCreate() {
        super.onCreate()
        injector = Injector(this)
    }
}