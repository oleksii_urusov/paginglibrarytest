package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.domain.common.usecase.PaginatedUseCase
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.NoArgsPageParameters
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedDataBuilder

open class UseCasePagedDataHelper<MODEL, RESPONSE_MODEL>
constructor(
    pagedListBuilder: PagedDataBuilder
) : BasePagedListDataHelper<MODEL, RESPONSE_MODEL>(pagedListBuilder) {

    protected lateinit var dataFetchUseCase: PaginatedUseCase<ResultObject<RESPONSE_MODEL>>
    protected lateinit var mappingFunction: (RESPONSE_MODEL) -> List<MODEL>

    fun createPagedLiveData(
        dataFetchUseCase: PaginatedUseCase<ResultObject<RESPONSE_MODEL>>,
        mapper: (RESPONSE_MODEL) -> List<MODEL>
    ): LiveData<PagedList<MODEL>> {
        this.dataFetchUseCase = dataFetchUseCase
        this.mappingFunction = mapper
        return getUpdatedPagedData().data
    }

    override fun fetch(
        startPosition: Int,
        loadSize: Int,
        isInitialLoad: Boolean,
        onSuccess: (RESPONSE_MODEL) -> Unit,
        onError: () -> Unit
    ) {
        val pageParams = NoArgsPageParameters(startPosition, loadSize)
        dataFetchUseCase(pageParams) { result ->
            when (result) {
                is ResultObject.Success -> onSuccess(result.data)
                is ResultObject.Error -> onError()
            }
        }
    }

    override fun RESPONSE_MODEL.mapToModelList(): List<MODEL> = mappingFunction(this)
}