package com.example.oleksiiurusov.paginglibrarytest.domain.github.model

interface PageParameters {
    val pageNumber: Int
    val pageSize: Int
}