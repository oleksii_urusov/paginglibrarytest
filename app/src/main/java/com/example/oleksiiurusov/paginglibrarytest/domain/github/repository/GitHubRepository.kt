package com.example.oleksiiurusov.paginglibrarytest.domain.github.repository

import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters

interface GitHubRepository {
    fun getRepositories(pageParameters: PageParameters): ResultObject<List<GitHubRepo>>
}
