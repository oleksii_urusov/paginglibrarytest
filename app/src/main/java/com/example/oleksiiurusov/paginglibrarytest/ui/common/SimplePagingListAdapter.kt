package com.example.oleksiiurusov.paginglibrarytest.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlin.reflect.KClass

private fun <T> defaultDiffCallback(areContentsSame: (T, T) -> Boolean) =
    object : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem === newItem
        override fun areContentsTheSame(oldItem: T, newItem: T) = areContentsSame(oldItem, newItem)
    }

class SimplePagingListAdapter<MODEL, VIEW_HOLDER : SimplePagingListAdapter.ViewHolder<MODEL>>(
    @LayoutRes private val itemLayoutId: Int,
    private val viewHolderClass: KClass<VIEW_HOLDER>,
    diffCallback: DiffUtil.ItemCallback<MODEL>,
    retryClickListener: () -> Unit,
    private val itemClickListener: (position: Int, item: MODEL) -> Unit
) : PagingListAdapter<MODEL>(diffCallback, retryClickListener) {

    constructor(
        @LayoutRes itemLayoutId: Int,
        viewHolderClass: KClass<VIEW_HOLDER>,
        areContentsSame: (MODEL, MODEL) -> Boolean,
        retryClickListener: () -> Unit,
        itemClickListener: (position: Int, item: MODEL) -> Unit
    ) : this(
        itemLayoutId = itemLayoutId,
        viewHolderClass = viewHolderClass,
        diffCallback = defaultDiffCallback(areContentsSame),
        retryClickListener = retryClickListener,
        itemClickListener = itemClickListener
    )

    override fun onCreateItemViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        LayoutInflater.from(parent.context)
            .inflate(itemLayoutId, parent, false)
            .createViewHolder()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            getItem(position)?.let { item ->
                itemClickListener(position, item)
            }
        }
        if (holder is ViewHolder<*>) {
            bindViewHolder(position, holder as VIEW_HOLDER)
        }
    }

    private fun bindViewHolder(position: Int, holder: VIEW_HOLDER) {
        holder.bind(getItem(position), position, itemClickListener)
    }

    abstract class ViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T?, position: Int, itemClickListener: (position: Int, item: T) -> Unit)
    }

    private fun View.createViewHolder() = viewHolderClass.java.getConstructor(View::class.java).newInstance(this)
}
