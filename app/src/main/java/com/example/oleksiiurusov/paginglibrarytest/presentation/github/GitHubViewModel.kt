package com.example.oleksiiurusov.paginglibrarytest.presentation.github

import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.domain.github.usecase.GetGitHubReposUseCase
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.BaseViewModel
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper.SimpleUseCasePagedHelper
import com.example.oleksiiurusov.paginglibrarytest.presentation.github.model.GitHubScreenNavigation
import com.example.oleksiiurusov.paginglibrarytest.presentation.github.model.GitHubScreenViewState

class GitHubViewModel(
    private val pagedListDataHelper: SimpleUseCasePagedHelper<GitHubRepo>,
    getGitHubReposUseCase: GetGitHubReposUseCase
) : BaseViewModel<GitHubScreenViewState, GitHubScreenNavigation>() {

    init {
        val reposPagedData = pagedListDataHelper.createPagedLiveData(getGitHubReposUseCase)
        viewState.addSource(reposPagedData) { updateState(newRepos = it) }
        viewState.addSource(pagedListDataHelper.listState) { updateState(newListState = it) }
    }

    fun onRefreshRepos() {
        pagedListDataHelper.reload()
    }

    fun onRetry() {
        pagedListDataHelper.retry()
    }

    private fun updateState(
        newRepos: PagedList<GitHubRepo>? = currentViewState?.gitHubRepos,
        newListState: ListStateUI = currentViewState?.listState ?: ListStateUI.Idle(true)
    ) {
        viewState.setValue(GitHubScreenViewState(newRepos, newListState))
    }
}