package com.example.oleksiiurusov.paginglibrarytest.domain.common.usecase

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseUseCase<IN_TYPE, OUT_TYPE> {
    private val job = Job()
    protected val uiScope = CoroutineScope(Dispatchers.Main + job)

    protected val isCallbackInitialised: Boolean
        get() = ::callback.isInitialized

    protected lateinit var callback: (OUT_TYPE) -> Unit

    protected abstract fun execute(value: IN_TYPE, callback: (OUT_TYPE) -> Unit)

    operator fun invoke(value: IN_TYPE, callback: (OUT_TYPE) -> Unit) {
        execute(value, callback)
    }

    open fun cancel() {
        job.cancel()
    }
}