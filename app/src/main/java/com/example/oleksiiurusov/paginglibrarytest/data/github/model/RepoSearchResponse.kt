package com.example.oleksiiurusov.paginglibrarytest.data.github.model

import com.google.gson.annotations.SerializedName

data class RepoSearchResponse(
    @SerializedName("total_count") val total: Int = 0,
    @SerializedName("items") val items: List<GitHubRepo> = emptyList(),
    val nextPage: Int? = null
)
