package com.example.oleksiiurusov.paginglibrarytest.common

sealed class ResultObject<out DATA_TYPE> {
    class Success<DATA_TYPE>(val data: DATA_TYPE) : ResultObject<DATA_TYPE>()
    class Error(val cause: GeneralCauseError) : ResultObject<Nothing>()
}

abstract class GeneralCauseError(message: String) : Throwable(message)

class UnknownError(message: String) : GeneralCauseError(message)