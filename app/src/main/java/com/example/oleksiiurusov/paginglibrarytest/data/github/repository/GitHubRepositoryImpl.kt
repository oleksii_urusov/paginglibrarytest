package com.example.oleksiiurusov.paginglibrarytest.data.github.repository

import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.data.github.datasource.network.RemoteGitHubDataSource
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters
import com.example.oleksiiurusov.paginglibrarytest.domain.github.repository.GitHubRepository

class GitHubRepositoryImpl(
    private val remoteDataSource: RemoteGitHubDataSource
) : GitHubRepository {

    override fun getRepositories(pageParameters: PageParameters): ResultObject<List<GitHubRepo>> {
        return remoteDataSource.getGitHubRepositories(pageParameters)
    }
}