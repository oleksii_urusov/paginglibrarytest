package com.example.oleksiiurusov.paginglibrarytest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.oleksiiurusov.paginglibrarytest.R
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.ui.common.SimplePagingListAdapter

class RepoViewHolder(view: View) : SimplePagingListAdapter.ViewHolder<GitHubRepo>(view) {

    private val name: TextView = view.findViewById(R.id.repo_name)
    private val description: TextView = view.findViewById(R.id.repo_description)
    private val stars: TextView = view.findViewById(R.id.repo_stars)
    private val language: TextView = view.findViewById(R.id.repo_language)
    private val forks: TextView = view.findViewById(R.id.repo_forks)

    override fun bind(item: GitHubRepo?, position: Int, itemClickListener: (position: Int, item: GitHubRepo) -> Unit) {
        if (item == null) {
            showDefaultData()
        } else {
            showRepoData(item)
        }
    }

    private fun showDefaultData() {
        val resources = itemView.resources
        name.text = resources.getString(R.string.loading)
        description.visibility = View.GONE
        language.visibility = View.GONE
        stars.text = resources.getString(R.string.unknown)
        forks.text = resources.getString(R.string.unknown)
    }

    private fun showRepoData(gitHubRepo: GitHubRepo) {
        name.text = gitHubRepo.fullName

        // if the description is missing, hide the TextView
        var descriptionVisibility = View.GONE
        if (gitHubRepo.description != null) {
            description.text = gitHubRepo.description
            descriptionVisibility = View.VISIBLE
        }
        description.visibility = descriptionVisibility

        stars.text = gitHubRepo.stars.toString()
        forks.text = gitHubRepo.forks.toString()

        // if the language is missing, hide the label and the value
        var languageVisibility = View.GONE
        if (!gitHubRepo.language.isNullOrEmpty()) {
            val resources = this.itemView.context.resources
            language.text = resources.getString(R.string.language, gitHubRepo.language)
            languageVisibility = View.VISIBLE
        }
        language.visibility = languageVisibility
    }

    companion object {
        fun create(parent: ViewGroup): RepoViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.repo_view_item, parent, false)
            return RepoViewHolder(view)
        }
    }
}