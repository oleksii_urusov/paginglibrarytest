package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI

class PagingCommonSourceFactory<MODEL>(
    private val dataFetchDelegate: DataFetchDelegate<MODEL>
) : DataSource.Factory<Int, MODEL>(), PagedDataLoadingManager {

    private val _dataSource = MutableLiveData<PagingCommonSource<MODEL>>()
    override val loadingState: LiveData<ListStateUI> = Transformations.switchMap(_dataSource) {
        it.loadingState
    }

    override fun create(): DataSource<Int, MODEL> =
        PagingCommonSource(dataFetchDelegate).also {
            _dataSource.postValue(it)
        }

    override fun reload() {
        _dataSource.value?.reload()
    }

    override fun retryFailedRequest() {
        _dataSource.value?.retryFailedRequest()
    }
}