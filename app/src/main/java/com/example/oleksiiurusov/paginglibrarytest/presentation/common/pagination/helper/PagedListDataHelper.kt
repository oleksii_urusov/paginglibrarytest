package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.NoArgsPageParameters
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedDataBuilder

open class PagedListDataHelper<MODEL, RESPONSE>
constructor(
    pagedDataBuilder: PagedDataBuilder
) : BasePagedListDataHelper<MODEL, RESPONSE>(pagedDataBuilder) {

    protected lateinit var dataFetchFunction: (
        pageParams: PageParameters,
        callback: (ResultObject<RESPONSE>) -> Unit
    ) -> Unit
    private lateinit var mappingFunction: (RESPONSE) -> List<MODEL>

    fun createPagedLiveData(
        mapper: (RESPONSE) -> List<MODEL>,
        getData: (request: PageParameters, callback: (ResultObject<RESPONSE>) -> Unit) -> Unit
    ): LiveData<PagedList<MODEL>> {
        this.dataFetchFunction = getData
        this.mappingFunction = mapper
        return getUpdatedPagedData().data
    }

    override fun fetch(
        startPosition: Int,
        loadSize: Int,
        isInitialLoad: Boolean,
        onSuccess: (RESPONSE) -> Unit,
        onError: () -> Unit
    ) {
        val pageParams = NoArgsPageParameters(startPosition, loadSize)

        dataFetchFunction(pageParams) { result ->
            when (result) {
                is ResultObject.Success -> onSuccess(result.data)
                is ResultObject.Error -> onError()
            }
        }
    }

    override fun RESPONSE.mapToModelList(): List<MODEL> = mappingFunction(this)
}