package com.example.oleksiiurusov.paginglibrarytest.data.common

import retrofit2.Call
import retrofit2.HttpException
import retrofit2.Response

private const val CODE_UNAUTHORIZED = 401
private const val CODE_INTERNAL_SERVER_ERROR = 500

fun <RESPONSE_TYPE, RESULT_TYPE> Call<RESPONSE_TYPE>.getResult(
    successConverter: (RESPONSE_TYPE) -> RESULT_TYPE,
    errorConverter: (String, Response<RESPONSE_TYPE?>?, Throwable?) -> RESULT_TYPE
): RESULT_TYPE = try {
    val response = execute()
    response.body()?.let(successConverter)
        ?: errorConverter("${response.code()} : ${response.message()}", response, null)
} catch (throwable: Throwable) {
    errorConverter(throwable.toErrorString(), null, throwable)
}

private fun Throwable.toErrorString() =
    when (this) {
        is HttpException -> "${code()}: $message"
        else -> localizedMessage ?: ""
    }

val Response<*>.isCodeUnauthorized
    get() = code() == CODE_UNAUTHORIZED

val Response<*>.isCodeServerError
    get() = code() == CODE_INTERNAL_SERVER_ERROR