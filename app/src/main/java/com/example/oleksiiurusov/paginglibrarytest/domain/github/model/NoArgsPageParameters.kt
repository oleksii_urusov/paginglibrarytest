package com.example.oleksiiurusov.paginglibrarytest.domain.github.model

data class NoArgsPageParameters(
    override val pageNumber: Int,
    override val pageSize: Int
) : PageParameters