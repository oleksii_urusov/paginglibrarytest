package com.example.oleksiiurusov.paginglibrarytest.data.github.datasource

import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters

interface GitHubDataSource {
    fun getGitHubRepositories(pageParameters: PageParameters): ResultObject<List<GitHubRepo>>
}