package com.example.oleksiiurusov.paginglibrarytest.ui.github

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.oleksiiurusov.paginglibrarytest.R
import com.example.oleksiiurusov.paginglibrarytest.common.BaseActivity
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI
import com.example.oleksiiurusov.paginglibrarytest.presentation.github.GitHubViewModel
import com.example.oleksiiurusov.paginglibrarytest.presentation.github.model.GitHubScreenViewState
import com.example.oleksiiurusov.paginglibrarytest.ui.RepoViewHolder
import com.example.oleksiiurusov.paginglibrarytest.ui.common.SimplePagingListAdapter
import com.example.oleksiiurusov.paginglibrarytest.ui.common.extensions.toGone
import com.example.oleksiiurusov.paginglibrarytest.ui.common.extensions.toVisible
import kotlinx.android.synthetic.main.activity_main.list_is_empty_retry_group as retryViewGroup
import kotlinx.android.synthetic.main.activity_main.progress_group as progressViewGroup
import kotlinx.android.synthetic.main.activity_main.swipeRefreshLayout

private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<GitHubRepo>() {
    override fun areItemsTheSame(oldItem: GitHubRepo, newItem: GitHubRepo): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: GitHubRepo, newItem: GitHubRepo): Boolean =
        oldItem == newItem
}

class MainActivity : BaseActivity() {
    private val viewModel: GitHubViewModel by lazy { getViewModel(GitHubViewModel::class.java) }
    private val adapter = SimplePagingListAdapter(
        itemLayoutId = R.layout.repo_view_item,
        viewHolderClass = RepoViewHolder::class,
        diffCallback = REPO_COMPARATOR,
        retryClickListener = { viewModel.onRetry() },
        itemClickListener = ::onItemClick
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rv: RecyclerView = findViewById(R.id.rv_repo_list)
        rv.adapter = adapter

        bindViewModel(viewModel)
    }

    private fun onItemClick(
        position: Int,
        item: GitHubRepo
    ) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(item.url))
        startActivity(intent)
    }

    private fun bindViewModel(viewModel: GitHubViewModel) {
        viewModel.viewState.observe(this, Observer { newState ->
            newState?.let { updateState(it) }
        })

        swipeRefreshLayout.setOnRefreshListener {
            viewModel.onRefreshRepos()
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun updateState(newState: GitHubScreenViewState) {
        adapter.submitList(newState.gitHubRepos)
        progressViewGroup.toGone()
        retryViewGroup.toGone()

        when (val listState = newState.listState) {
            is ListStateUI.Idle -> adapter.setIdleState()
            is ListStateUI.Loading -> onStateLoading(listState)
            is ListStateUI.Error -> onStateError(listState)
        }
    }

    private fun onStateError(newState: ListStateUI) {
        if (newState.listIsEmpty) {
            retryViewGroup.toVisible()
        } else {
            adapter.setRetryState()
        }
    }

    private fun onStateLoading(newState: ListStateUI) {
        if (newState.listIsEmpty) {
            progressViewGroup.toVisible()
        } else {
            adapter.setLoadingState()
        }
    }
}
