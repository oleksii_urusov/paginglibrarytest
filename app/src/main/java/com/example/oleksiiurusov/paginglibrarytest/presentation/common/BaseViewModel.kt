package com.example.oleksiiurusov.paginglibrarytest.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.NavigationEvent
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ViewState

abstract class BaseViewModel<VIEW_STATE : ViewState, NAVIGATION_EVENT : NavigationEvent> : ViewModel() {
    val viewState: LiveData<VIEW_STATE> = MediatorLiveData()
    val navigationEvent: LiveData<NAVIGATION_EVENT> = MediatorLiveData()

    protected val currentViewState
        get() = viewState.value

    protected fun <T> LiveData<T>.setValue(value: T) {
        if (this is MutableLiveData<T>) { this.value = value }
    }

    protected fun <T> LiveData<T>.postValue(value: T) {
        if (this is MutableLiveData<T>) { postValue(value) }
    }

    protected fun <TYPE, SOURCE_TYPE> LiveData<TYPE>.addSource(
        sourceLiveData: LiveData<SOURCE_TYPE>,
        onChanged: (SOURCE_TYPE) -> Unit
    ) {
        if (this is MediatorLiveData) { addSource(sourceLiveData, onChanged) }
    }
}