package com.example.oleksiiurusov.paginglibrarytest.data.github.datasource.network

import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.common.UnknownError
import com.example.oleksiiurusov.paginglibrarytest.data.common.getResult
import com.example.oleksiiurusov.paginglibrarytest.data.github.datasource.GitHubDataSource
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.RepoSearchResponse
import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters
import retrofit2.Response

private const val DEFAULT_REPO_QUERY = "Android"

class RemoteGitHubDataSource(private val githubService: GithubService) : GitHubDataSource {

    override fun getGitHubRepositories(pageParameters: PageParameters) = githubService.searchRepos(
        DEFAULT_REPO_QUERY,
        pageParameters.pageNumber,
        pageParameters.pageSize
    ).getResult(
        successConverter = { ResultObject.Success(it.items) },
        errorConverter = ::defaultErrorConverter
    )

    private fun defaultErrorConverter(
        message: String,
        response: Response<RepoSearchResponse?>?,
        throwable: Throwable?
    ) = ResultObject.Error(UnknownError(message))
}