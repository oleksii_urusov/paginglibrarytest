package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.common.ResultObject
import com.example.oleksiiurusov.paginglibrarytest.domain.common.usecase.PaginatedUseCase
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedDataBuilder

class SimpleUseCasePagedHelper<MODEL>
constructor(
    pagedListBuilder: PagedDataBuilder
) : UseCasePagedDataHelper<MODEL, List<MODEL>>(pagedListBuilder) {

    fun createPagedLiveData(dataFetchUseCase: PaginatedUseCase<ResultObject<List<MODEL>>>): LiveData<PagedList<MODEL>> {
        this.dataFetchUseCase = dataFetchUseCase
        return getUpdatedPagedData().data
    }

    override fun List<MODEL>.mapToModelList(): List<MODEL> = this // No need for mapping
}