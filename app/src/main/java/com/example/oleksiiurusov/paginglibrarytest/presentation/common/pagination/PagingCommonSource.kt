package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI

class PagingCommonSource<MODEL>(
    private val dataFetchDelegate: DataFetchDelegate<MODEL>
) : PositionalDataSource<MODEL>() {

    private var failedRequest: (() -> Unit)? = null
    private val _loadingState = MutableLiveData<ListStateUI>().apply {
        value = ListStateUI.Idle(true)
    }
    val loadingState: LiveData<ListStateUI> = _loadingState

    private val isCurrentListEmpty: Boolean
        get() = loadingState.value?.listIsEmpty ?: true

    fun retryFailedRequest() {
        failedRequest?.invoke()
    }

    fun reload() {
        invalidate()
    }

    @MainThread
    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<MODEL>) {
        setLoadingState(true)
        dataFetchDelegate.requestPageData(
            startPosition = 0,
            loadSize = params.requestedLoadSize,
            isInitialLoad = true,
            onSuccess = {
                callback.onResult(it, 0)
                setIdleState(it.isEmpty())
            },
            onError = {
                failedRequest = { loadInitial(params, callback) }
                setErrorState(true)
            }
        )
    }

    @MainThread
    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<MODEL>) {
        setLoadingState(isCurrentListEmpty)
        dataFetchDelegate.requestPageData(
            startPosition = params.startPosition,
            loadSize = params.loadSize,
            isInitialLoad = false,
            onSuccess = { newList ->
                callback.onResult(newList)
                setIdleState(newList.isEmpty() && isCurrentListEmpty)
            },
            onError = {
                failedRequest = { loadRange(params, callback) }
                setErrorState(false)
            }
        )
    }

    private fun setErrorState(listIsEmpty: Boolean) {
        _loadingState.value = ListStateUI.Error(listIsEmpty)
    }

    private fun setIdleState(listIsEmpty: Boolean) {
        _loadingState.value = ListStateUI.Idle(listIsEmpty)
    }

    private fun setLoadingState(listIsEmpty: Boolean) {
        _loadingState.value = ListStateUI.Loading(listIsEmpty)
    }
}