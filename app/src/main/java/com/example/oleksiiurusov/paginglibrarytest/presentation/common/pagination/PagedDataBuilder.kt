package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import java.util.concurrent.Executor

class PagedDataBuilder(private val config: PagedList.Config) {

    private val fetchExecutor = object : Executor { // Use Main thread executor since threading is handled by UseCase
        private val handler = Handler(Looper.getMainLooper())
        override fun execute(command: Runnable?) {
            handler.post(command)
        }
    }

    fun <VALUE> build(dataFetchDelegate: DataFetchDelegate<VALUE>): PagedData<VALUE> {
        val sourceFactory = PagingCommonSourceFactory(dataFetchDelegate)
        val pagedList = sourceFactory.toLiveData(
            config = config,
            fetchExecutor = fetchExecutor
        )
        return PagedData(
            data = pagedList,
            loadingManager = sourceFactory
        )
    }
}

data class PagedData<MODEL>(
    val data: LiveData<PagedList<MODEL>>,
    val loadingManager: PagedDataLoadingManager
)