package com.example.oleksiiurusov.paginglibrarytest.presentation.common.model

sealed class ListStateUI(val listIsEmpty: Boolean) {
    class Idle(isEmpty: Boolean) : ListStateUI(isEmpty)
    class Loading(isEmpty: Boolean) : ListStateUI(isEmpty)
    class Error(isEmpty: Boolean) : ListStateUI(isEmpty)
}