package com.example.oleksiiurusov.paginglibrarytest.common

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.oleksiiurusov.paginglibrarytest.di.CustomViewModelFactory
import com.example.oleksiiurusov.paginglibrarytest.di.PagingTestApplication

@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity() {

    protected lateinit var viewModelFactory: CustomViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val injector = (application as PagingTestApplication).injector
        viewModelFactory = CustomViewModelFactory(injector)
    }

    protected fun <T : ViewModel> getViewModel(viewModelClass: Class<T>) =
        ViewModelProviders.of(this, viewModelFactory).get(viewModelClass)
}