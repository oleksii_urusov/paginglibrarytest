package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination

interface DataFetchDelegate<T> {
    fun requestPageData(
        startPosition: Int,
        loadSize: Int,
        isInitialLoad: Boolean,
        onSuccess: (List<T>) -> Unit,
        onError: () -> Unit
    )
}