package com.example.oleksiiurusov.paginglibrarytest.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CustomViewModelFactory(private val injector: Injector) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = injector.inject(modelClass)
}