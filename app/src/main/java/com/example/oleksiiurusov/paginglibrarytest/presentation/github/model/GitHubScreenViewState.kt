package com.example.oleksiiurusov.paginglibrarytest.presentation.github.model

import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.data.github.model.GitHubRepo
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ViewState

data class GitHubScreenViewState(
    val gitHubRepos: PagedList<GitHubRepo>?,
    val listState: ListStateUI
) : ViewState