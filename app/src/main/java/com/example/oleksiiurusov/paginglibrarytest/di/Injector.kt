package com.example.oleksiiurusov.paginglibrarytest.di

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.oleksiiurusov.paginglibrarytest.data.github.datasource.network.GithubService
import com.example.oleksiiurusov.paginglibrarytest.data.github.datasource.network.RemoteGitHubDataSource
import com.example.oleksiiurusov.paginglibrarytest.data.github.repository.GitHubRepositoryImpl
import com.example.oleksiiurusov.paginglibrarytest.domain.github.usecase.GetGitHubReposUseCase
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.PagedDataBuilder
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination.helper.SimpleUseCasePagedHelper
import com.example.oleksiiurusov.paginglibrarytest.presentation.github.GitHubViewModel

private const val PAGE_SIZE = 10
private const val INITIAL_LOAD_BUFFER = 10
private const val PREFETCH_DISTANCE = 1

class Injector(private val application: Application) {
    private val pagedDataBuilder: PagedDataBuilder
        get() = PagedDataBuilder(pagedListConfig)

    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setInitialLoadSizeHint(PAGE_SIZE + INITIAL_LOAD_BUFFER)
        .setPageSize(PAGE_SIZE)
        .setPrefetchDistance(PREFETCH_DISTANCE)
        .build()

    private val gitHubRepository
        get() = GitHubRepositoryImpl(gitHubRemoteDataSource)

    private val gitHubRemoteDataSource
        get() = RemoteGitHubDataSource(GithubService.create())

            @SuppressLint("Unchecked cast")
            fun <T : ViewModel?> inject(modelClass: Class<T>): T = when {
                modelClass.isAssignableFrom(GitHubViewModel::class.java) ->
                    GitHubViewModel(pagedUseCaseDataHelper(), GetGitHubReposUseCase(gitHubRepository)) as T
                else -> throw IllegalArgumentException("Unknown ViewModel class")
            }

    private fun <T> pagedUseCaseDataHelper() = SimpleUseCasePagedHelper<T>(pagedDataBuilder)
}