package com.example.oleksiiurusov.paginglibrarytest.presentation.common.pagination

import androidx.lifecycle.LiveData
import com.example.oleksiiurusov.paginglibrarytest.presentation.common.model.ListStateUI

interface PagedDataLoadingManager {

    val loadingState: LiveData<ListStateUI>

    fun reload()

    fun retryFailedRequest()
}