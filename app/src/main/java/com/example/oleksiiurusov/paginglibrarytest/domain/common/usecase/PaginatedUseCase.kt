package com.example.oleksiiurusov.paginglibrarytest.domain.common.usecase

import com.example.oleksiiurusov.paginglibrarytest.domain.github.model.PageParameters

abstract class PaginatedUseCase<RESPONSE> : BackgroundExecuteUseCase<PageParameters, RESPONSE>()